<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

</head>
<body>
<div class="flex-center position-ref full-height">
     <div class="container">
         <div class="row">
             <div class="col">
                 <h1 ><a href="{{route('home')}}">Deadlinks search</a> </h1>
                 <hr>
             </div>
         </div>
         <div class="row">
             <div class="col">
             <form action="" >
                 <div class="input-group mb-3">
                     <input type="text" name="url" value="{{request('url')}}" class="form-control" placeholder="Url" aria-label="Url" aria-describedby="button-addon2">
                     <input type="text" name="status" value="{{request('status')}}" class="form-control" placeholder="Status" aria-label="Status" aria-describedby="button-addon2">
                     <div class="input-group-append">
                         <button class="btn btn-outline-primary" type="submit" id="button-addon2">Search</button>
                     </div>
                 </div>
             </form>
             </div>

         </div>
         <div class="row">
             <div class="col">
                 @if(count($results)>0)
                 <table class="table table-bordered">
                     <thead>
                     <tr>
                         <th scope="col">Found Url</th>
                         <th scope="col">Deadlink</th>
                         <th scope="col">Status</th>
                         <th scope="col">Time</th>
                     </tr>
                     </thead>
                     <tbody>
                     @foreach($results as $result)
                     <tr>
                         <td>{{$result->url}}</td>
                         <td scope="col">{{$result->deadlink}}</td>
                         <td scope="col">{{$result->status}}</td>
                         <td>{{date('d.m.Y H:i:s',strtotime($result->created_at))}}</td>
                     </tr>
                         @endforeach
                     </tbody>
                 </table>
                     @else
                     <div class="alert alert-warning">No deadlinks found yet. Try crawling domains by console application!</div>
                 @endif
                     {{ $results->links() }}
             </div>
         </div>
     </div>
</div>
</body>
</html>
