<?php

namespace App\Console\Commands;

use App\CrawlLogger;
use Illuminate\Console\Command;
use Spatie\Browsershot\Browsershot;
use Spatie\Crawler\CrawlAllUrls;
use Spatie\Crawler\Crawler;
use Spatie\Crawler\CrawlSubdomains;
use GuzzleHttp\RequestOptions;
use Symfony\Component\Console\Question\ConfirmationQuestion;

class DeadLinks extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crawler:start {url} {--javascript=n}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Crawling websites and finding deadlinks';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $baseUrl = $this->argument('url');
        $crawlProfile = new CrawlAllUrls($baseUrl);
        $crawlLogger = new CrawlLogger($this);
        $this->info("Start scanning {$baseUrl}");
        $this->line('');

        $clientOptions = [
            RequestOptions::TIMEOUT => 10,
            RequestOptions::VERIFY => false,
            RequestOptions::ALLOW_REDIRECTS => false,
        ];
        $browsershot = new Browsershot();

        $crawler = Crawler::create($clientOptions)
            ->setConcurrency(10)
            ->setCrawlObserver($crawlLogger)
            ->setDelayBetweenRequests(150)
            ->setCrawlProfile($crawlProfile);
        if ($this->option('javascript') == 'y') {
            $crawler->setBrowsershot($browsershot)
                ->executeJavaScript();
        }

        $crawler->ignoreRobots();


        $crawler->startCrawling($baseUrl);

        return 0;

    }
}
