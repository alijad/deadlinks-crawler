<?php

namespace App\Http\Controllers;

use App\Deadlink;
use Illuminate\Http\Request;

class CrawlController extends Controller
{
   public function index(Request $request)
   {
       $results=Deadlink::orderBy('created_at','DESC');
       if(isset($request->url))
       {
           $url=$request->url;
           $results=$results->where('url','like',"%$url%");
       }
       if(isset($request->status))
       {
           $status=$request->status;
           $results=$results->where('status','like',"%$status%");
       }
       $results=$results->paginate(20);

       return view('crawl.index',compact('results'));
   }
}
